
const express = require("express")

const app = express()

const port = 3000;

app.use(express.json())
app.use(express.urlencoded({extended: true}));


// GET
app.get('/', (request, response) => {

	response.send('Hello World')

})


app.get('/hello', (request, response) => {
	response.send('Hello from the "/hello" endpoint')

})


// POST

app.post('/hello', (request, response) => {
	response.send(`Hello there, ${request.body.firstName} ${request.body.lastName}! This is from the "/hello" endpoint but with a post method`)

})

/*
	{
		"firstName": "Iyan",
		"lastName": "De Leon"
	}
*/




//  REGISTER

let users = []


app.post('/register', (request, response) => {
	
	if (request.body.username !== '' && request.body.password !== ''){
		users.push(request.body)
		response.send(`User ${request.body.username} successfully registered`)
		console.log(request.body)
	} else {
			response.send(`Please input BOTH username and password`)
	}

	

})

/*
	{
		"username": "Iyan",
		"password": " "
	}
*/

// Change password

app.use('/change-password', (request, response) => {

	let message;

	for (let i = 0; i < users.length; i++) {
		if (request.body.username == users[i].username) {
			users[i].password = request.body.password
			message = `User ${request.body.username}'s password has been updated`

			break;
		} else {
			message = "User does not exist"
		}
	}

			response.send(message)
	

	

})




app.listen(port, () => console.log(`Server running at port ${port}`))
